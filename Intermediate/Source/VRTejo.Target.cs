using UnrealBuildTool;

public class VRTejoTarget : TargetRules
{
	public VRTejoTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("VRTejo");
	}
}
